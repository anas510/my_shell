*************************************Version-1*********************************************

=> I have handled variable length string input and also variable length arguments without any limit on size of argument as size of arguments is also increase dynamically according to requirement without wasting any extra space.

*************************************Version-2*********************************************

=> I have also handled error in case if input file not found or if user has redirected input/output more than once.

=>  Redirection arguments should be at the end of parsed string otherwise it won't work properly

=>  IPC pipe is also working perfectly exactly like in bash shell

=>  Also both Redirection and Pipe can be used simultaneously and error message is also displayed properly is case of wrong input

=> If both redirection and pipe are used then 1st pipe will be implemented then Redirection because BASH also perform these tasks in this sequence. So in case if we give command:

	                        cat < f1 > f2 | wc 

then output will be :		0	0	0				

because no input will reach wc as output of cat will be redirected to f2.

*************************************Version-3*********************************************

=> Commands work exactly as they should is background and also when a background process terminates a message is displayed on stdout with PID of terminated child using SIGCHILD handler.

=>  Multiple commands work exactly like in BASH and a process can also be executed in background using multiple commands in a single line correctly like in BASH.

=>  But I have not handled IO Redirection in multi-command line, although it can be easily done as I have implemented separate functions to handle Pipes and IO Re-directions.

*************************************Version-4*********************************************

=>  History file saves at max last 100 commands using "stifle_history(100)"

=>  Up/Down arrow keys can also be used here to traverse between saved history commands

=>  In case of multiple commands seperated by ;, each command is saved as a seperate entry in history

=>  Also extra TABS and spaces will be removed from command before saving in history file using mergeArgs() function.

=>  Input validation is also performed for executing some previous command only !-100 to !100 is valid as input.

=>  Also !0 is invalid as input.

=>  Filename completion feature on TAB is also added in this version.

=>  inputString() function has been deleted as readline() is much better than that.

=>  Shell is terminated only if CTRL+D is pressed on empty line just like BASH Shell.


*************************************Version-5*********************************************

=> I have also implemented else part i.e complete if-then-else-fi

=> Also performed a-lot of input validation for if-else-then-fi and displayed proper error 
messages

=> It 1st executes command/commands after "if" keyword and if the return value is 0 then 
   executes command after "then" keyword otherwise command after "else" if given otherwise 
   do nothing.

=>  A command can't be executed in background in if command. Option to execute command in 
    background is available only for simple, or multiple commands seperated by commas. 
    (i.e. not for pipes and if)

*************************************Version-6*********************************************

=> I have also implemented "time" command that displays current system time in long-format.

=> For saving background jobs I have created a Doubly-Linked-List that saves JobID and 
   ProcessID of each background process and afterwards search for processes in that list.

=> If a process that we want to send signal is found then it will be killed using SIGKILL 
   and removed from list

=> Almost all errors and input validations has been done efficiently and I have tested many scenarios. It is working for all inputs correctly.

=> I am not saving command/processName in DList so in the output of "jobs" command only jobid and pid is displayed :		

	[jobID]		PID
	
*************************************Version-7*********************************************

=> Everything is working according to the given specification for version-7.

=> Variable should be written as name=value (without any space in-between)

=> Doubly-Linked List is implemented for stoarage of variables.

=> At the end of program all global variables are stored in a file named "globals" and are 
   retrieved from it at start of program

=> Following built-in commands have been implemented for handling shell variables:

	set:		to set local variables	

	get:		to get local variables				

	del:		delete local variable with entered name

	locals:		display all local variables

	setglobal:      to set global variable				

	getglobal:      to get global variable

	delglobal:	delete global variable with entered name

	globals:	display all global variables

	showall:	display all variables

=> While getting or deleting a variable only its name should be passed without '='

=> History is not handled during script file execution

=> Filename completion feature on Tab is also added.

=> Internal commands can also be used in script file and with if-structure.

=>  Added support for multiple pipes.

=>	Terminating 'less' cmd on CTRL+C (SIGINT)


=> I have used following resources for help and guidance;

	=> some posts of stackoverflow

	=> https://linux.die.net/man/3/history  for managing history properly

	=> http://man7.org/linux/man-pages/man3/readline.3.html   for a little help about 
           readline behavior

	=> Man pages

	=> Some posts of geeksforgeeks.com

	=> And few others