#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <functions.h>
#include <DList.h>

extern int argsCount;
extern int jNo;
extern struct DList* bgList;

int isBuiltin(char* cmd)
{
	if(strstr(cmd,"jobs")!=NULL || strstr(cmd,"cd")!=NULL || strstr(cmd,"kill")!=NULL 
		|| strstr(cmd,"exit")!=NULL || strstr(cmd,"help")!=NULL || strstr(cmd,"time")!=NULL)
		return 0;
	return -1;
}

int handleBuiltInCmds(char** argList)
{
	if(strstr(argList[0],"cd")!=NULL)
	{
		if(argsCount>=2)
		{
			chdir(argList[1]);
			return 1;
		}
		else
			fprintf(stderr, "Error: Incomplete arguments...'cd' command requires atleast 1 argument\n");
	}
	else if(strstr(argList[0],"exit")!=NULL)
	{
		if(argsCount==1)
		{
			fprintf(stdout, "exit");		//BASH prints this
			return 2;
		}
		else
			fprintf(stderr, "Error: Too many arguments...'exit' command requires no argument\n");
	}
	else if(strstr(argList[0],"help")!=NULL)
	{
		if(argsCount==1)
		{
			printHelp();		//displays help from a file named help
		}
		else
			fprintf(stderr, "Error: Too many arguments...'help' command requires no argument\n");
	}
	else if(strstr(argList[0],"time")!=NULL)
	{
		if(argsCount==1)
		{
			time_t mytime;
		    mytime = time(NULL);
		    fprintf(stdout,"%s",ctime(&mytime));
		}
		else
			fprintf(stderr, "Error: Too many arguments...'time' command requires no argument\n");
	}
	else if(strstr(argList[0],"jobs")!=NULL)
	{
		if(argsCount==1)
		{
			dlist_printList(bgList);
		}
		else
			fprintf(stderr, "Error: Too many arguments...'jobs' command requires no argument\n");
	}
	else if(strstr(argList[0],"kill")!=NULL)
	{
		if(argsCount==2)
		{
			int pid=0,jid=0;

			//now parse 2nd argument of kill
			int n=parseArg(argList[1],&pid,&jid);
			
			if(n==-1)
			{
				fprintf(stderr, "Error: Invalid JobID or PID\n");
				return -1;	
			}
			int cpid=dlist_findData(bgList,pid,jid);
			if(cpid>0)
			{
				kill(cpid,SIGKILL);
				fprintf(stdout,"Killed process %d\n",cpid);
			}
			else
			{
				fprintf(stdout,"No background process found with this ID\n");				
			}
		}
		else
			fprintf(stderr, "Error: Incorrect arguments...'kill' command requires only 1 argument\n");
	}	

	return 0;
}

void printHelp()
{
	FILE* fp = fopen("help","r");

	char buffer[256];
	while (fgets(buffer, 256, fp)!=NULL){
	   fputs(buffer, stdout);
	}
	fprintf(stdout,"\n");
	fclose(fp);
}

int parseArg(char* arg,int * pid,int *jid)
{
	if(arg!=NULL)
	{
		int l=strlen(arg);
		char* num=(char*)malloc(l);
		int i=0,j=0,k=0;
		if(arg[0]=='%')		//for jid
		{
			j=1;
			i++;
		}
		for(;i<l;i++,k++)
		{
			if(isDigit(arg[i])!=0)		//character is not a digit
			{
				free(num);
				return -1;			//invalid format
			}
			else
			{
				num[k]=arg[i];
			}
		}

		int n=atoi(num);
		free(num);
		if(j==1)
		{
			*pid=0;
			*jid=n;
		}
		else
		{
			*pid=n;
			*jid=0;
		}
		return 0;	
	}

	return -1;
}