#include <stdio.h>
#include <stdlib.h>
#include <functions.h>
#include <readline/readline.h>
#include <readline/history.h>

int isDigit(char c)
{
	switch(c)
	{
		case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
			return 0;
		default:
			return -1;
	}
}

int getHistoryNumber(char* cmd)
{
	int len=strlen(cmd);
	char* num=(char*)malloc(len);
	int i=1,m=1,j=0;
	
	if(cmd[1]=='-')
	{
		m=-1;	
		i=2;
	}

	for(;i<len;i++,j++)
	{
		if(isDigit(cmd[i])==0)
			num[j]=cmd[i];
		else
		{
			free(num);
			return 11;
		}
	}
	num[len-1]='\0';

	int n=atoi(num);
	free(num);
	return n*m;
}
