#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <DList.h>

extern int jNo;

void dlist_const(struct DList* dl)
{
	dl->head = (struct Node*)calloc(1,sizeof(struct Node));		//Dummy Nodes
	dl->tail = (struct Node*)calloc(1,sizeof(struct Node));
	
	dl->head->pid=0;
	dl->tail->pid=0;
	dl->head->jid=0;
	dl->tail->jid=0;
	
	dl->head->next = dl->tail;
	dl->tail->prev = dl->head;
}

void dlist_dest(struct DList* dl)
{
	struct Node* t;
	while (dl->head!=NULL)
	{
		t = dl->head;
		dl->head = dl->head->next;
		free(t);
	}

	dl->head = dl->tail = NULL;
}

//-1 means notEmpty
// 1 means empty
short dlist_isEmpty(struct DList* dl)
{
	if (dl->head!=NULL && dl->tail!=NULL)		//If destructor is called explicitly
		return (dl->head->next == dl->tail? 1 : -1);	
	else
		return 1;
}

void dlist_insertAtLast(struct DList* dl, int pid,int jid)
{
	struct Node* t = (struct Node*)calloc(1,sizeof(struct Node));
	t->pid=pid;
	t->jid=jid;

	t->next = dl->tail;
	t->prev = dl->tail->prev;
	dl->tail->prev->next = t;
	dl->tail->prev = t;
}

void dlist_remove(struct DList* dl, struct Node* y)
{
	if (y != dl->head && y != dl->tail && y!=NULL)
	{
		y->prev->next = y->next;
		y->next->prev = y->prev;
		free(y);
	}
}

//returns PID for killing process and if node is found then remove Node before returning
//because if a process is found then that will surely be killed using SIGKILL
int dlist_findData(struct DList* dl, int pid,int jid)		
{
	struct Node* current = dl->head->next;	
	while (current != dl->tail)
	{
		if ((jid==0 && pid >0 && pid == current->pid) || (pid==0 && jid >0 && jid == current->jid))
		{
			pid=current->pid;
			dlist_remove(dl,current);		//Remove current from dlist
			return pid;
		}
		current = current->next;
	}

	return 0;				
}

//for "jobs" command
void dlist_printList(struct DList* dl)
{
	if (dlist_isEmpty(dl)==1)
		fprintf(stdout,"Currently, the background-processes list is empty.\n");
	else
	{
		struct Node* temp = dl->head->next;
		while (temp != dl->tail)
		{
			fprintf(stdout,"[%d]\t%d\n",temp->jid,temp->pid);
			temp = temp->next;
		}
	}
}