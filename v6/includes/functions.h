#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <signal.h>

char* inputString(char* , FILE* );
char** tokenize(char*);
char* mergeArgs(char**,int);

int executeCmd(char**);
int executePipe(char**);
char*** handlePipe(char**);		//it will return 2 char** lists
void handleRedirection(char**);
void freeMemory(char**,char*);

int isBuiltin(char*);
int handleBuiltInCmds(char**);
void printHelp();

int isMultiple(char**);
int isPipeUsed(char**);
int isBackGround(char**);
char*** seperateCmds(char**);		//seperate cmds if written with ;
char*** extractCmds(char**);

int getHistoryNumber(char*);
//0 = success, -1=false
int isDigit(char);
int parseArg(char* arg,int * pid,int *jid);		//extract jid or pid for kill command from argument

void handleSig(int);
void handleSigINT(int);
void handleParentSig(int);
void sigCHILD(int , siginfo_t *, void *);

#endif