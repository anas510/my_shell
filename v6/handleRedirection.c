#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <functions.h>

extern int argsCount;
extern short pipeUsed;
void handleRedirection(char** argList)
{
	short ip=0,op=0;
	int i=0;
	while(argList[i]!=NULL)
	{
		if(strstr(argList[i],"<")!=NULL)		//input redirection
		{
			if(ip==1)
			{
				fprintf(stderr,"Error:	You have redirected input more than once.");
				exit(1);
			}
			i++;
			int fd=open(argList[i],O_RDONLY);
			if(fd==-1)
			{
				perror("Input redirection failed");
				exit(1);
			}
			dup2(fd,0);
			close(fd);
			ip=1;		//input is redirected now
		}
		else if(strstr(argList[i],">")!=NULL)		//output redirection
		{
			if(op==1)
			{
				fprintf(stderr,"Error:	You have redirected output more than once.");
				exit(1);
			}
			i++;
			int fd=open(argList[i],O_WRONLY|O_CREAT,0664);
			if(fd==-1)
			{
				perror("Output redirection failed");
				exit(1);
			}
			dup2(fd,1);
			close(fd);
			op=1;		//output is redirected now
		}
		if(ip==1 && op==1)
		{
			break;
		}
		i++;
	}
	int l=-1;
	while(argList[++l]!=NULL);
	if(ip==1)
	{
		l-=2;
		if(l>=0)
		{
			argList[l]=NULL;
			if(pipeUsed==1)
				argsCount-=2;
		}
		else
		{
			fprintf(stderr,"Invalid input for redirection");
			exit(1);
		}
	}
	if(op==1)
	{
		l-=2;
		if(l>=0)
		{
			argList[l]=NULL;
			if(pipeUsed==1)
				argsCount-=2;
		}	
		else
		{
			fprintf(stderr,"Invalid input for redirection");
			exit(1);
		}
	}
}

int isPipeUsed(char** argList)
{
	int i=0;
	while(argList[i]!=NULL)
	{
		if(strstr(argList[i],"|")!=NULL)		//input redirection
		{
			if(i!=argsCount-1)
				return 1;
			else
				return -1;
		}
		i++;
	}
	return 0;
}

int isBackGround(char** argList)
{
	int i=0;
	while(argList[i]!=NULL)
	{
		if(strstr(argList[i],"&")!=NULL)		//input redirection
		{
			//Don't decrease argsCount here otherwise Segmentation fault will occur in deallocation
			argList[i]='\0';		
			return 1;		
		}
		i++;
	}
	return 0;
}