#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <setjmp.h>
#include <fcntl.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <functions.h>
#include <header.h>
#include <DList.h>

extern int argsCount;		//stores number of arguments in string found after parsing
extern short pipeUsed;
static jmp_buf envbuf;
extern struct DList* bgList;

int main()
{
	//To make shell prompt
	char prompt[] = "cs321@";
	char pwd[4096];		//MAX Possible Linux PATH_LENGTH
	getcwd(pwd,4096);
	strcat(prompt,pwd);
	strcat(prompt,":- ");

	signal(SIGINT, &handleParentSig);			

	//Initialize list for bgTasks 	(jobs,kill)
	bgList=(struct DList*)calloc(1,sizeof(struct DList));
	dlist_const(bgList);
	
	read_history("history");		//to read ./history file
	stifle_history(10);				//limits history size to 10
	//in start history_base is at 1	

	char* inputLine;
	char* cmd=NULL;
	char** argList;
	
s:	while(setjmp(envbuf)>=0 && (inputLine=readline(prompt))!=NULL)		//Returns NULL if EOF is encountered on an empty line
	{
		if((argList=tokenize(inputLine))!=NULL)
		{
			//Handle external commands
			if(isBuiltin(argList[0])!=0)
			{
				//To handle History
				if(argsCount==1 && argList[0][0]=='!')
				{
					int histNo=getHistoryNumber(argList[0]);
	        		if(histNo<-10 || histNo>10)
	        		{
	        			fprintf(stderr,"Error: Invlaid history number...history_offset can be from -10 to 10 only\n");
						freeMemory(argList,inputLine);
	        			goto s;
	        		}
	        		else
	        		{
	        			if(histNo<0)
	        				histNo+=history_length+1;        			
	        			
	        			//On each insertion history_base increases as max limit of 10 has already reached
	        			histNo+=history_base-1;			
	        			struct _hist_entry* hE=history_get(histNo);
	        			if(hE!=NULL)
	        			{
							freeMemory(argList,inputLine);
							/*Without this when free(inputLine) will be called hE->line will
							 be freed and garbage will remain in history at that offset*/
							char* newCmd=(char*)malloc(1);
							newCmd[0]='\0';
							strcpy(newCmd,hE->line);
	            			inputLine=newCmd;		//overwrite command to rexecute old command
							if((argList=tokenize(inputLine))==NULL)
								goto s;
	        			}

	        		}
				}
				int rv=1;
				if(strstr(argList[0],"if")==NULL)
				{
					rv=isPipeUsed(argList);
					if(rv==0)
					{
						char*** cmds=seperateCmds(argList);		//Seperate commands if multiple are written seperated with ;
						int i=0;

						while(cmds[i]!=NULL)
						{
							cmd=mergeArgs(cmds[i],strlen(inputLine));	
			        		add_history(cmd);
			        		free(cmd);
			        		executeCmd(cmds[i]);
			        		i++;
						}
						free(cmds);
					}
					else if (rv==1)
					{
						pipeUsed=1;
		        		cmd=mergeArgs(argList,strlen(inputLine));		//Generate command by combining all arguments with spaces
						executePipe(argList);
					}
					else
					{
						fprintf(stderr,"Invalid input: there is no command after |\n");
		        		cmd=mergeArgs(argList,strlen(inputLine));		//Generate command by combining all arguments with spaces
					}				
					if(rv!=0)
		        	{
		        		add_history(cmd);	
		        		free(cmd);			
		        	}
		        }
		        else		//For if control structure
		        {
		        	cmd=mergeArgs(argList,strlen(inputLine));
		        	char*** cmds=extractCmds(argList);		//Seperate commands after if, then and else keyword
	    			if(cmds==NULL)
					{
						fprintf(stderr,"Error: Invalid format for if control structure\n");
						freeMemory(argList,inputLine);
						goto s;
					}
					else
					{
			     		add_history(cmd);	
						
						//To execute if,then else
						if(executeCmd(cmds[0])==0)	        	
						{
							//success so execute then
							executeCmd(cmds[1]);
						}
						else if(cmds[2]!=NULL)		//Else
						{
							executeCmd(cmds[2]);						
						}
						free(cmds);
					}
					free(cmd);				
		        }			
	    	}
	    	else
	    	{
	    		cmd=mergeArgs(argList,strlen(inputLine));	
        		add_history(cmd);
        		free(cmd);
	    		//Handle builtin commands
	    		//Builtin cmds can't be Redirected or used with pipes
	    		int status=handleBuiltInCmds(argList);
	    		if(status==2)		//exit
				{
					freeMemory(argList,inputLine);
					break;
				}
				else if(status==1)		//cd
				{
					strcpy(prompt,"cs321@");
					getcwd(pwd,4096);
					strcat(prompt,pwd);
					strcat(prompt,":- ");
				}

	    	}
			freeMemory(argList,inputLine);			
		}
	}
    
    //Write in history file before terminating
    write_history("history");
	fprintf(stdout,"\n");
	return 0;	
}

//Why is it working only for 1st signal?
void handleParentSig(int sig)
{
	fprintf(stdout,"\n");
	signal(SIGINT, &handleParentSig);			
	longjmp(envbuf,sig);
}

void freeMemory(char** argList, char* inputLine)
{
	//To free memory
	for(int i=0;i<argsCount;i++)
	{
		if(argList[i]!=NULL)
			free(argList[i]);
	}
	pipeUsed=0;
	free(argList);
	free(inputLine);
	argsCount=0;
}