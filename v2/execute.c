#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <functions.h>

int executeCmd(char** argList)
{
	int status;
	//struct sigaction sa;
	int cpid=fork();
	switch(cpid)
	{
		case -1:
			perror("Fork failed");
			exit(1);
			break;
		case 0:
			//All this is useless here because after exec new code will replace all this
			//So we can't write sig_handlers for childs that will exec
			// sa.sa_handler=&handleSigINT;
			// sa.sa_flags=SA_RESTART;
			// sigemptyset(&sa.sa_mask);
			// sigaction(SIGINT, &sa,NULL);		

			// signal(SIGINT,&handleSigINT);
			
			handleRedirection(argList);
			execvp(argList[0],argList);		//name of child(2nd parameter) will also be argList[0]
			perror("execvp failed");
			exit(1);
			break;
		default:
			if(waitpid(cpid,&status,0)==-1)
				perror("waitpid failed");
			//to print '\n' if child is terminated by a signal
			handleSig(status);
			return status;
	}
}

void handleSig(int status)		//To print \n after a process is terminated by SIGINT
{
	if (WIFSIGNALED(status))
		fprintf(stdout,"\n");
}

void handleSigINT(int sig)
{
	fprintf(stdout,"Caught signal =%d\n",sig);
	fprintf(stdout,"\n");
	exit(0);
}