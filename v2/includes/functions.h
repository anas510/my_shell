#ifndef FUNCTIONS_H
#define FUNCTIONS_H

char* inputString(char* , FILE* );
char** tokenize(char*);
int executeCmd(char**);
int executePipe(char**);
char*** handlePipe(char**);		//it will return 2 char** lists
void handleRedirection(char**);

int isPipeUsed(char**);

void handleSig(int);
void handleSigINT(int);
void handleParentSig(int);

#endif