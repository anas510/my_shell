#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <functions.h>

extern int argsCount;
extern int lessPid;

char*** handlePipe(char** argList,int* count)
{
	int size=1;
	char*** cmds=(char***)malloc(sizeof(char**)*(size+1));
	cmds[0]=argList;
	cmds[1]='\0';
	
	int k=0,j=0;
	for(int i=0;i<argsCount;i++)
	{
		if(strstr(argList[i],"|")!=NULL)
		{
			cmds[k][j]='\0';
			j=0;
			if(i!=argsCount-1)		//input redirection
			{
				cmds=(char***)realloc(cmds,sizeof(char**)*(++size+1));
				cmds[++k]=&argList[i+1];
				cmds[k+1]='\0';
			}
		}
		else
			j++;
	}
	// fprintf(stderr,"k = %d, argsCount = %d, j = %d",k,argsCount,j);
	*count=size;
	return cmds;
}

int executePipe(char** argList,int* pipeOutFd,int last)
{
	int status;
	// fprintf(stderr,"pipeOutFd=%d\n",*pipeOutFd);
	int fd[2];

	if(last==0)
	{
		if(pipe(fd)<0)		//opens 2 descriptors for pipe 	fd[0] = pipeInput, fd[1]=pipeOutput
		{
			perror("Pipe failed");
			return 1;
		}
		// fprintf(stderr,"fd[0]=%d,\tfd[1]=%d\n",fd[0],fd[1]);
	}

	int cpid=fork();
	
	//For handling SIGINT in less
	struct sigaction sa,old;
	sa.sa_handler=&handleSigINT;
	sa.sa_flags=SA_RESTART;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGINT, &sa,&old);

	save_term_attr();
	switch(cpid)
	{
		case -1:
			perror("Fork failed");
			exit(1);
			break;
		case 0:
			//shell handles pipe before redirection because if we run command with both redirection and pipe
			//output will not goto 2nd command as it will be redirected
			if(*pipeOutFd!=-1)			//For 1st command
			{
				dup2(*pipeOutFd,0);		//Read input from output of previous command
			}

			if(last==0)
			{
				close(fd[0]);
				//REDIRECT OUTPUT of 1st 2 fd[1]
				dup2(fd[1],1);
			}
			//Then check for IO-Redirection
			handleRedirection(argList);
			
			execvp(argList[0],argList);

			perror("execvp failed");
			exit(1);
			break;
		default:
			if(strcmp(argList[0],"less")==0)		//If command is less then save its PID for killing on CTRL+C
				lessPid=cpid;

			if(waitpid(cpid,&status,0)==-1)
				perror("waitpid failed");
			handleSig(status);
			
			sigaction(SIGINT, &old, NULL);			//Reset old handler again
			
			if(*pipeOutFd!=-1)
			{
				close(*pipeOutFd);
			}
			if(last==0)		
			{
				//close fd[1] otherwise next command will be blocked in waiting forever
				close(fd[1]);
				*pipeOutFd=fd[0];
			}	
			else
			{
				*pipeOutFd=-1;
			}
	}

	reset_term();
	
	return status;
}