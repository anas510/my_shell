#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <setjmp.h>
#include <fcntl.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <functions.h>
#include <header.h>
#include <DList.h>
#include <varDList.h>

extern int argsCount;		//stores number of arguments in string found after parsing
static jmp_buf envbuf;
extern struct DList* bgList;
extern struct VarList* varList;
extern int lessPid;

extern char** environ;
int main(int argc,char* argv[])
{
	if(argc==1)
	{
		//To make shell prompt
		char prompt[] = "\033[01m\033[32mcs321\033[33m@";
		//To get OS Name	
		FILE *fp;
	    char buffer[50] = " ";
	    fp = popen("lsb_release -ds", "r");
	    fgets(buffer, 50, fp);
	    pclose(fp);
	  	//to seperate OS Name  
	    char* osN=strtok(buffer," ");
		osN[0]=tolower(osN[0]);

	    char pwd[4096];		//MAX Possible Linux PATH_LENGTH
		getcwd(pwd,4096);

		char os[]="hello";
		strcpy(os,osN);
		strcat(prompt,os);
		strcat(prompt,":\033[36m");
		strcat(prompt,pwd);
		strcat(prompt,"\033[39m\033[0m$ ");

		struct sigaction new;
		new.sa_handler=&handleParentSig;
		new.sa_flags=SA_RESTART;
		sigemptyset(&new.sa_mask);
		sigaction(SIGINT,&new,NULL);
					
		//Initialize list for bgTasks 	(jobs,kill)
		bgList=(struct DList*)calloc(1,sizeof(struct DList));
		dlist_const(bgList);
		
		//Initialize list for variables
		varList=(struct VarList*)calloc(1,sizeof(struct VarList));
		varlist_const(varList);
		varlist_readGlobals(varList);
		
		read_history("history");		//to read ./history file
		
		stifle_history(100);				//limits history size to 100
		
		//in start history_base is at 1	

		char* cmd=NULL;
		char* inputLine;
		char** argList;
		
	s:	while(setjmp(envbuf)>=0 && (inputLine=readline(prompt))!=NULL)		//Returns NULL if EOF is encountered on an empty line
		{
			if((argList=tokenize(inputLine))!=NULL)
			{
				//Handle external commands
				if(isBuiltin(argList[0])!=0)
				{
					//To handle History
					if(argsCount==1 && argList[0][0]=='!')
					{
						int histNo=getHistoryNumber(argList[0]);
		        		if(histNo<-100 || histNo>100 || histNo==0)
		        		{
		        			fprintf(stderr,"Error: Invlaid history number...history_offset can be from -100 to 100 only and it can't be 0\n");
							freeMemory(argList,inputLine);
		        			goto s;
		        		}
		        		else
		        		{
		        			if(histNo<0)
		        				histNo+=history_length+1;        			
		        			
		        			//On each insertion history_base increases as max limit of 10 has already reached
		        			histNo+=history_base-1;			
		        			struct _hist_entry* hE=history_get(histNo);
		        			if(hE!=NULL)
		        			{
								freeMemory(argList,inputLine);
								/*Without this when free(inputLine) will be called hE->line will
								 be freed and garbage will remain in history at that offset so declare
								 inputLine again in a new seperate variable*/
								char* newCmd=(char*)malloc(1);
								newCmd[0]='\0';
								strcpy(newCmd,hE->line);
		            			inputLine=newCmd;		//overwrite command to rexecute old command
								if((argList=tokenize(inputLine))==NULL)
									goto s;
		        			}

		        		}
					}
					int rv=1;
					if(strstr(argList[0],"if")==NULL)
					{
						rv=isPipeUsed(argList);
						if(rv==0)
						{
							char*** cmds=seperateCmds(argList);		//Seperate commands if multiple are written seperated with ;
							int i=0;

							while(cmds[i]!=NULL)
							{
								cmd=mergeArgs(cmds[i],strlen(inputLine));	
				        		add_history(cmd);
				        		free(cmd);
				        		executeCmd(cmds[i]);
				        		i++;
							}
							free(cmds);
						}
						else if (rv==1)
						{
			        		cmd=mergeArgs(argList,strlen(inputLine));		//Generate command by combining all arguments with spaces for history

							int count=0,i=0,last=0;
							int pipeOutFd=-1;
							char*** cmds=handlePipe(argList,&count);

							while(cmds[i]!=NULL)
							{
								if(i+1==count)
									last=1;
								executePipe(cmds[i],&pipeOutFd,last);
								// fprintf(stderr,"pipeOutFd=%d\n",pipeOutFd);

				        		i++;
							}
							free(cmds);

						}
						else
						{
							fprintf(stderr,"Invalid input: there is no command after |\n");
			        		cmd=mergeArgs(argList,strlen(inputLine));		//Generate command by combining all arguments with spaces
						}				
						if(rv!=0)
			        	{
			        		add_history(cmd);	
			        		free(cmd);			
			        	}
			        }
			        else		//For if control structure
			        {
			        	cmd=mergeArgs(argList,strlen(inputLine));
			        	char*** cmds=extractCmds(argList);		//Seperate commands after if, then and else keyword
		    			if(cmds==NULL)
						{
							fprintf(stderr,"Error: Invalid format for if control structure\n");
							freeMemory(argList,inputLine);
							goto s;
						}
						else
						{
				     		add_history(cmd);	
							int r=executeCmd(cmds[0]);
							if(r==-2)
							{
								free(cmds);
								freeMemory(argList,inputLine);
			        			break;		//exit has been called
							}
							else if(r!=-1 && r!=256)		//256 for exit	
							{
								//success so execute then
								if(executeCmd(cmds[1])==-2)
								{
									free(cmds);
									freeMemory(argList,inputLine);
				        			break;		//exit has been called
								}
							}
							else if(cmds[2]!=NULL)		//Else part
							{
								if(executeCmd(cmds[2])==-2)
								{
									free(cmds);
									freeMemory(argList,inputLine);
				        			break;		//exit has been called
								}							
							}
							free(cmds);
						}
						free(cmd);				
			        }			
		    	}
		    	else 	//Handle builtin commands
		    	{
		    		cmd=mergeArgs(argList,strlen(inputLine));	
	        		add_history(cmd);
	        		free(cmd);
		    		//Builtin cmds can't be Redirected or used with pipes
		    		int status=handleBuiltInCmds(argList);
		    		if(status==2)		//exit
					{
						freeMemory(argList,inputLine);
						break;
					}
					else if(status==1)		//cd
					{
						//To make shell prompt
						memset(prompt,'\0',strlen(prompt));
						strcpy(prompt,"\033[01m\033[32mcs321\033[33m@");
						getcwd(pwd,4096);
						strcat(prompt,os);
						strcat(prompt,":\033[36m");
						strcat(prompt,pwd);
						strcat(prompt,"\033[39m\033[0m$ ");
					}
		    	}
				freeMemory(argList,inputLine);
				inputLine=NULL;			
			}
		}
		
		//Save global variables before terminating    
		varlist_writeGlobals(varList);
	    //Write in history file before terminating
	    write_history("history");
	}
	else if(argc>1 && argc<=3)
	{
		if(argc==2)
		{
			fprintf(stderr,"Error: Invalid arguments\n");
		}
		else if(argc==3)
		{
			if(strcmp(argv[1],"-f")==0)
			{
				//Initialize list for variables
				varList=(struct VarList*)calloc(1,sizeof(struct VarList));
				varlist_const(varList);
				varlist_readGlobals(varList);
				
				//handle script file execution
				executeScriptFile(argv[2]);
				varlist_writeGlobals(varList);
			}
			else
			{
				fprintf(stderr,"Error: Invalid arguments\n");		
			}
		}
	}
	else
	{
		fprintf(stderr,"You can pass at-max 2 arguments for executing script files with '-f' otherwise no argument.\n");
	}
	fprintf(stdout,"\n");
	return 0;	
}

//Why is it working only for 1st signal?
void handleParentSig(int sig)
{
	fprintf(stdout,"\n");
	longjmp(envbuf,sig);
}

void freeMemory(char** argList, char* inputLine)
{
	//To free memory
	for(int i=0;i<argsCount;i++)
	{
		if(argList[i]!=NULL)
			free(argList[i]);
	}
	free(argList);
	free(inputLine);
	argsCount=0;
}