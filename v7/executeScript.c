#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>
#include <functions.h>
#include <varDList.h>
#include <DList.h>

extern int argsCount;		//stores number of arguments in string found after parsing
extern struct VarList* varList;

void executeScriptFile(char* file)
{
	FILE* fp = fopen(file,"r");
	if(fp==NULL)
	{
		fprintf(stderr,"fopen failed for '%s' :",file);
		perror("");
		return;
	}
	char** argList;
	char inputLine[256];
sh:		
	while (fgets(inputLine, 256, fp)!=NULL)
	{
		int l=strlen(inputLine);

		if(l==0)
			continue;
		if(inputLine[l-1]=='\n')
			inputLine[l-1]='\0';

		if((argList=tokenize(inputLine))!=NULL)
		{
			//Handle external commands
			if(isBuiltin(argList[0])!=0)
			{
				int rv=1;
				if(strstr(argList[0],"if")==NULL)
				{
					rv=isPipeUsed(argList);
					if(rv==0)
					{
						char*** cmds=seperateCmds(argList);		//Seperate commands if multiple are written seperated with ;
						int i=0;

						while(cmds[i]!=NULL)
						{
			        		if(executeCmd(cmds[i])==-2)
			        			goto e;		//exit has been called
			        		i++;
						}
						free(cmds);
					}
					else if (rv==1)
					{
						int count=0,i=0,last=0;
						int pipeOutFd=-1;
						char*** cmds=handlePipe(argList,&count);

						while(cmds[i]!=NULL)
						{
							if(i+1==count)
								last=1;
							executePipe(cmds[i],&pipeOutFd,last);

			        		i++;
						}
						free(cmds);
					}
					else
					{
						fprintf(stderr,"Invalid input: there is no command after |\n");
					}				
		        }
		        else		//For if control structure
		        {

		        	char*** cmds=extractCmds(argList);		//Seperate commands after if, then and else keyword
	    			
	    			if(cmds==NULL)
					{
						fprintf(stderr,"Error: Invalid format for if control structure\n");
						freeMemory(argList,(char*)malloc(1));
						strcpy(inputLine,"");
						goto sh;
					}
					else
					{
						//To execute if,then else
						int r=executeCmd(cmds[0]);
						if(r==-2)
						{
							free(cmds);
							freeMemory(argList,(char*)malloc(1));
		        			break;		//exit has been called
						}
						else if(r!=-1 && r!=256)		//256 for exit	
						{
							//success so execute then
							if(executeCmd(cmds[1])==-2)
							{
								free(cmds);
								freeMemory(argList,(char*)malloc(1));
			        			break;		//exit has been called
							}
						}
						else if(cmds[2]!=NULL)		//Else part
						{
							r=executeCmd(cmds[2]);
							if(r==-2)
							{
								free(cmds);
								freeMemory(argList,(char*)malloc(1));
			        			break;		//exit has been called
							}							
						}
						free(cmds);
						strcpy(inputLine,"");
					}
		        }			
	    	}
	    	else 	//Handle internal commands
	    	{
	    		//Handle builtin commands
	    		//Builtin cmds can't be Redirected or used with pipes
	    		int status=handleBuiltInCmds(argList);
	    		if(status==2)		//exit
				{
					freeMemory(argList,(char*)malloc(1));
					break;
				}
	    	}
			freeMemory(argList,(char*)malloc(1));
		}   
	}

e:	fclose(fp);
}