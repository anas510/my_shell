#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <varDList.h>

void varlist_const(struct VarList* dl)
{
	dl->head = (struct variable*)calloc(1,sizeof(struct variable));		//Dummy variables
	dl->tail = (struct variable*)calloc(1,sizeof(struct variable));
	
	dl->head->global=0;
	dl->tail->str=NULL;
	dl->head->global=0;
	dl->tail->str=NULL;
	
	dl->head->next = dl->tail;
	dl->tail->prev = dl->head;
}

void varlist_dest(struct VarList* dl)
{
	struct variable* t;
	while (dl->head!=NULL)
	{
		t = dl->head;
		dl->head = dl->head->next;
		free(t->str);
		free(t);
	}

	dl->head = dl->tail = NULL;
}

//-1 means notEmpty
// 1 means empty
short varlist_isEmpty(struct VarList* dl)
{
	if (dl->head!=NULL && dl->tail!=NULL)		//If destructor is called explicitly
		return (dl->head->next == dl->tail? 1 : -1);	
	else
		return 1;
}

void varlist_insert(struct VarList* dl, char* str,int g)
{
	char* temp=(char*)malloc(strlen(str));
	strcpy(temp,str);
	char* name=strtok(temp,"=");
	struct variable* result=varlist_findVariable(dl,name,g);
	if(result!=NULL)		//If variable is already in list then just update str
	{
		result->str=(char*)realloc(result->str,strlen(str));
		strcpy(result->str,str);
	}
	else
	{
		struct variable* t = (struct variable*)calloc(1,sizeof(struct variable));
		
		char* var=(char*)malloc(strlen(str));		//Otherwise when we delete argList it will become dangling
		strcpy(var,str);
		t->str=var;
		t->global=g;

		t->next = dl->tail;
		t->prev = dl->tail->prev;
		dl->tail->prev->next = t;
		dl->tail->prev = t;
	}
	free(temp);
}

struct variable* varlist_findVariable(struct VarList* dl, char* name,int g)		
{
	struct variable* current = dl->head->next;
	while (current != dl->tail)
	{
		char* temp=(char*)malloc(strlen(current->str));
		strcpy(temp,current->str);
		char* var=strtok(temp,"=");
		if (strcmp(name,var)==0 && current->global==g)
		{
			return current;
		}
		current = current->next;
	}
	return NULL;
}

void varlist_remove(struct VarList* dl, struct variable* y)
{
	if (y != dl->head && y != dl->tail && y!=NULL)
	{
		y->prev->next = y->next;
		y->next->prev = y->prev;
		free(y->str);
		free(y);
	}
}

//returns variable->str  as(name=val)
char* varlist_findData(struct VarList* dl, char* name,int g)		
{
	struct variable* current = dl->head->next;
	while (current != dl->tail)
	{
		char* temp=(char*)malloc(strlen(current->str));
		strcpy(temp,current->str);
		char* var=strtok(temp,"=");
		if (strcmp(name,var)==0 && current->global==g)
		{
			return current->str;
		}
		current = current->next;
	}

	char* c=(char*)malloc(1);
	c[0]='\0';
	return c;
}

int varlist_findAndDelete(struct VarList* dl, char* name,int g)		
{
	struct variable* current = dl->head->next;
	while (current != dl->tail)
	{
		char* temp=(char*)malloc(strlen(current->str));
		strcpy(temp,current->str);
		char* var=strtok(temp,"=");
		if (strcmp(name,var)==0 && current->global==g)
		{
			varlist_remove(dl,current);
			return 0;
		}
		current = current->next;
	}

	return -1;
}

//for "locals" command
void varlist_printLocals(struct VarList* dl)
{
	if (varlist_isEmpty(dl)==1)
		fprintf(stdout,"\nCurrently, the variables list is empty.\n");
	else
	{
		struct variable* temp = dl->head->next;
		while (temp != dl->tail)
		{
			if(temp->global==0)
			{
				fprintf(stdout,"%s\n",temp->str);
			}
			temp = temp->next;
		}
	}
}

//for "globals" command
void varlist_printGlobals(struct VarList* dl)
{
	if (varlist_isEmpty(dl)==1)
		fprintf(stdout,"\nCurrently, the variables list is empty.\n");
	else
	{
		struct variable* temp = dl->head->next;
		while (temp != dl->tail)
		{
			if(temp->global==1)
			{
				fprintf(stdout,"%s\n",temp->str);
			}
			temp = temp->next;
		}
	}
}

//for "showall" command
void varlist_printList(struct VarList* dl)
{
	if (varlist_isEmpty(dl)==1)
		fprintf(stdout,"\nCurrently, the variables list is empty.\n");
	else
	{
		struct variable* temp = dl->head->next;
		while (temp != dl->tail)
		{
			fprintf(stdout,"%s\n",temp->str);
			temp = temp->next;
		}
	}
}

//To save globals in file named globals
void varlist_writeGlobals(struct VarList* dl)
{
	if(varlist_isEmpty(dl)!=1)
	{
		FILE* fp = fopen("globals","w");
		if(fp==NULL)
		{
			perror("fopen failed for globals");
			return;
		}
		struct variable* temp = dl->head->next;
		while (temp != dl->tail)
		{
			if(temp->global==1)
			{
			   fprintf(fp,"%s\n", temp->str);
			}		   
			temp = temp->next;
		}
		fclose(fp);
	}
}

//To read globals from a file named globals
void varlist_readGlobals(struct VarList* dl)
{
	FILE* fp = fopen("globals","r");
	if(fp==NULL)
	{
		perror("fopen failed for globals");
		return;
	}
	char buffer[256];
	while (fgets(buffer, 256, fp)!=NULL)
	{
		int l=strlen(buffer);
		if(l>2)
		{
			buffer[l-1]='\0';
	 		varlist_insert(dl,buffer,1);
		}
	}

	fclose(fp);
}