#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <functions.h>
#include <DList.h>
#include <varDList.h>

extern int jNo;
extern struct DList* bgList;
extern struct VarList* varList;

int isBuiltin(char* cmd)
{
	if(strcmp(cmd,"jobs")==0 || strcmp(cmd,"cd")==0 || strcmp(cmd,"kill")==0 
		|| strcmp(cmd,"exit")==0 || strcmp(cmd,"help")==0 || strcmp(cmd,"time")==0
		|| strcmp(cmd,"get")==0 || strcmp(cmd,"set")==0 || strcmp(cmd,"del")==0
		|| strcmp(cmd,"locals")==0 || strcmp(cmd,"setglobal")==0 || strcmp(cmd,"delglobal")==0
		|| strcmp(cmd,"globals")==0 || strcmp(cmd,"getglobal")==0 || strcmp(cmd,"showall")==0)
		return 0;
	return -1;
}

int handleBuiltInCmds(char** argList)
{
	//To calculate arguments in argList
	//Because in case of scriptFile argsCount can be greater than the actual arguments
	int argCount=-1;
	while(argList[++argCount]!=NULL);

	if(strcmp(argList[0],"cd")==0)
	{
		if(argCount>=2)
		{
			chdir(argList[1]);
			return 1;
		}
		else
			fprintf(stderr, "Error: Incomplete arguments...'cd' command requires atleast 1 argument\n");
	}
	else if(strcmp(argList[0],"exit")==0)
	{
		if(argCount==1)
		{
			fprintf(stdout, "exit");		//BASH prints this
			return 2;
		}
		else
			fprintf(stderr, "Error: Too many arguments...'exit' command requires no argument\n");
	}
	else if(strcmp(argList[0],"help")==0)
	{
		if(argCount==1)
		{
			printHelp();		//displays help from a file named help
		}
		else
			fprintf(stderr, "Error: Too many arguments...'help' command requires no argument\n");
	}
	else if(strcmp(argList[0],"time")==0)
	{
		if(argCount==1)
		{
			time_t mytime;
		    mytime = time(NULL);
		    fprintf(stdout,"%s",ctime(&mytime));
		}
		else
			fprintf(stderr, "Error: Too many arguments...'time' command requires no argument\n");
	}
	else if(strcmp(argList[0],"jobs")==0)
	{
		if(argCount==1)
		{
			dlist_printList(bgList);
		}
		else
			fprintf(stderr, "Error: Too many arguments...'jobs' command requires no argument\n");
	}
	else if(strcmp(argList[0],"kill")==0)
	{
		if(argCount==2)
		{
			int pid=0,jid=0;

			//now parse 2nd argument of kill
			int n=parseArg(argList[1],&pid,&jid);
			
			if(n==-1)
			{
				fprintf(stderr, "Error: Invalid JobID or PID\n");
				return -1;	
			}
			int cpid=dlist_findData(bgList,pid,jid);
			if(cpid>0)
			{
				kill(cpid,SIGKILL);
				fprintf(stdout,"Killed process %d\n",cpid);
			}
			else
			{
				fprintf(stdout,"No background process found with this ID\n");				
			}
		}
		else
			fprintf(stderr, "Error: Incorrect arguments...'kill' command requires only 1 argument\n");
	}
	else	//Commands For managing variables
	{
		if(strcmp(argList[0],"locals")==0 || strcmp(argList[0],"globals")==0 || strcmp(argList[0],"showall")==0)
		{
			//For printing all variables
			if(argCount>1)
			{
				fprintf(stderr, "Error: Too many arguments...'locals', 'globals' and 'showall' command requires no argument\n");
				return -1;
			}
			else
			{
				if(argList[0][0]=='l')
				{
					varlist_printLocals(varList);
				}
				else if(argList[0][0]=='g')
				{
					varlist_printGlobals(varList);
				}
				else
				{
					varlist_printList(varList);
				}
			}
		}
		else
		{
			if(argCount!=2)
			{
				fprintf(stderr, "Error: Incorrect arguments...this command takes only 1 argument\n");
				return -1;
			}
			if(strstr(argList[0],"set")!=NULL)
			{
				if(strstr(argList[1],"=")==NULL)		//Error variable name has no =
				{
					fprintf(stderr, "Error: For setting variable, argument should be in format 'name=value'\n");
					return -1;		
				}
			}
			else
			{
				if(strstr(argList[1],"=")!=NULL)		//Error variable name has =
				{
					fprintf(stderr, "Error: For setting variable, argument should be in format 'name' without =\n");
					return -1;		
				}
			}

			//Input validation is done now handle commands
			if(strcmp(argList[0],"set")==0)
			{
				varlist_insert(varList,argList[1],0);
			}
			else if(strcmp(argList[0],"setglobal")==0)
			{
				varlist_insert(varList,argList[1],1);
			}
			else if(strcmp(argList[0],"getglobal")==0)
			{
				fprintf(stdout,"%s",varlist_findData(varList,argList[1],1));
			}
			else if(strcmp(argList[0],"get")==0)
			{
				fprintf(stdout,"%s",varlist_findData(varList,argList[1],0));
			}
			else if(strcmp(argList[0],"del")==0)
			{
				int r=varlist_findAndDelete(varList,argList[1],0);
				if(r==0)
				{
					fprintf(stdout, "Variable is successfully removed\n");
				}
				else
				{
					fprintf(stderr, "No variable found in list with this name.\n");
				}
			}
			else if(strcmp(argList[0],"delglobal")==0)
			{
				int r=varlist_findAndDelete(varList,argList[1],1);
				if(r==0)
				{
					fprintf(stdout, "Variable is successfully removed\n");
				}
				else
				{
					fprintf(stderr, "No variable found in list with this name.\n");
				}
			}
		}
	}

	return 0;
}

void printHelp()
{
	FILE* fp = fopen("help","r");
	if(fp==NULL)
	{
		perror("fopen failed for 'help'");
		return;
	}
	char buffer[256];
	while (fgets(buffer, 256, fp)!=NULL){
	   fputs(buffer, stdout);
	}
	fprintf(stdout,"\n");
	fclose(fp);
}

int parseArg(char* arg,int * pid,int *jid)
{
	if(arg!=NULL)
	{
		int l=strlen(arg);
		char* num=(char*)malloc(l);
		int i=0,j=0,k=0;
		if(arg[0]=='%')		//for jid
		{
			j=1;
			i++;
		}
		for(;i<l;i++,k++)
		{
			if(isDigit(arg[i])!=0)		//character is not a digit
			{
				free(num);
				return -1;			//invalid format
			}
			else
			{
				num[k]=arg[i];
			}
		}

		int n=atoi(num);
		free(num);
		if(j==1)
		{
			*pid=0;
			*jid=n;
		}
		else
		{
			*pid=n;
			*jid=0;
		}
		return 0;	
	}

	return -1;
}