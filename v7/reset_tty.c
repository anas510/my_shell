#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//This is to remember original terminal attributes. For changing mode to canonical
struct termios saved_attributes;

short attSet=0, input_mode=0;
//attSet to check whether termios variables are initialized or not
//input_mode=0 means program is in canonical mode (normal mode)

void reset_term()
{
   if(input_mode==1){
      if(!isatty (STDIN_FILENO)){
        //FOR IO Redirection
         FILE* fp=fopen("/dev/tty","r+");   
         tcsetattr (fileno(fp), TCSANOW, &saved_attributes);
      }
      else{
         tcsetattr (STDIN_FILENO, TCSANOW, &saved_attributes);    
      }
      input_mode=0;
   }
}

void save_term_attr()
{
   if(input_mode==0){
      struct termios tattr;
      int fileNo;
      /* Make sure stdin is a terminal. */
      if (!isatty (STDIN_FILENO) && !attSet)
      {
         //This is for handling terminal in case of IO REDIRECTION

         FILE* fp=fopen("/dev/tty","r+");
         attSet=1;
         //To get attributes of file with descriptor fileNo which is "/dev/tty"
         fileNo=fileno(fp);
         tcgetattr (fileNo, &saved_attributes);
         tcgetattr (fileNo, &tattr);
         
         fclose(fp);
      }
      else{
         tcgetattr (STDIN_FILENO, &saved_attributes);
         tcgetattr (STDIN_FILENO, &tattr);
      }      
   }
   input_mode=1;
}