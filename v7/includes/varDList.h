#ifndef VARDLIST_H
#define VARDLIST_H

struct variable
{
	int global;		//1=global, 0=local
	char* str;
	struct variable* next,*prev;
};

struct VarList
{
	struct variable* head,*tail;
};

void varlist_const(struct VarList* dl);
void varlist_dest(struct VarList* dl);
//-1 means notEmpty
// 1 means empty
short varlist_isEmpty(struct VarList* dl);
void varlist_insert(struct VarList* dl, char* str,int g);
//returns struct variable*
struct variable* varlist_findVariable(struct VarList* dl, char* name,int g);
void varlist_remove(struct VarList* dl, struct variable* y);
//returns variable->str  as(name=val)
int varlist_findAndDelete(struct VarList* dl, char* name,int g);	
//To remove variable
char* varlist_findData(struct VarList* dl, char* name,int g);
//for "locals" command
void varlist_printLocals(struct VarList* dl);
//for "globals" command
void varlist_printGlobals(struct VarList* dl);
//for "showall" command
void varlist_printList(struct VarList* dl);
//To save globals in file named globals
void varlist_writeGlobals(struct VarList* dl);
//To read globals from a file named globals
void varlist_readGlobals(struct VarList* dl);

#endif