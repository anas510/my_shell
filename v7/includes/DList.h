#ifndef DLIST_H
#define DLIST_H

struct Node
{
	int pid;
	int jid;
	struct Node* next,*prev;
};

struct DList
{
	struct Node* head,*tail;
};

void dlist_const(struct DList* dl);

void dlist_dest(struct DList* dl);

//for "jobs" command
void dlist_printList(struct DList* dl);

//returns PID for killing process and if node is found then remove Node before returning
//because if a process is found then that will surely be killed using SIGKILL
int dlist_findData(struct DList* dl, int pid,int jid);

void dlist_remove(struct DList* dl, struct Node* y);

void dlist_insertAtLast(struct DList* dl, int pid,int jid);

//-1 means notEmpty
// 1 means empty
short dlist_isEmpty(struct DList* dl);

#endif