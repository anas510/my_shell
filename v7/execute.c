#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <functions.h>
#include <DList.h>

extern int jNo;
extern int lessPid;
extern struct DList* bgList;

int executeCmd(char** argList)
{
	if(isBuiltin(argList[0])!=0)
	{
		int status;
		int cpid=fork();

		struct sigaction sa,old;
		sa.sa_handler=&handleSigINT;
		sa.sa_flags=SA_RESTART;
		sigemptyset(&sa.sa_mask);
		sigaction(SIGINT, &sa,&old);

		save_term_attr();
		int bg=isBackGround(argList);
		if(bg==-1)
		{
			fprintf(stderr,"Error: To run a process in backgroung & should be at the END.");
			return 1;
		}
		switch(cpid)
		{
			case -1:
				perror("Fork failed");
				break;
			case 0:
				handleRedirection(argList);
				execvp(argList[0],argList);		//name of child(2nd parameter) will also be argList[0]
				perror("execvp failed");
				exit(1);
			default:
				if(strcmp(argList[0],"less")==0)		//If command is less then save its PID for killing on CTRL+C
					lessPid=cpid;
				if(bg==1)
				{
					fprintf(stdout,"[%d]\t%d\n",++jNo,cpid);

					dlist_insertAtLast(bgList,cpid,jNo);
					
					sa.sa_sigaction=&sigCHILD;
					sa.sa_flags=SA_SIGINFO|SA_RESTART;
					sigaction(17, &sa,NULL);			//SIGCHILD=17
				}
				else
				{
					if(waitpid(cpid,&status,0)==-1)
						perror("waitpid failed");
					//to print '\n' if child is terminated by a signal
					handleSig(status);
				}
				reset_term();
				return status;
		}
	}
	else		//To handle Internal commands in script file
	{
		int status=handleBuiltInCmds(argList);
		if(status==2)
			return -2;
		else
			return 0;
	}
	return -1;
}

void handleSig(int status)		//To print \n after a process is terminated by SIGINT
{
	if (WIFSIGNALED(status))
		fprintf(stdout,"\n");
}

void handleSigINT(int sig)
{
	if(lessPid!=0)
	{
		kill(lessPid,9);
		lessPid=0;
	}	
}

//For background processes
void sigCHILD(int sig, siginfo_t *siginfo, void *context)
{
	int status;
	jNo--;
	pid_t pid=waitpid(-1, &status, WNOHANG);		//to wait for termination of any child
	if(pid>0)
		fprintf(stdout,"\nProcess: %d has terminated\n",pid);	
}