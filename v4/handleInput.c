#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

extern int argsCount;		//stores number of arguments in string found after parsing

char** tokenize(char* line)
{
	/*	allocate memory	*/
	/*Here line==NULL can never occur because readline() returns NULL
	 on EOF and in that case our program will already be terminated*/
	int len=strlen(line);
	if(len==0)
		return NULL;
	char** words = (char**)malloc(sizeof(char*));		
	words[0] = (char*)malloc(sizeof(char));			//For 1st argument
	words[0][0]='\0';

	int i=0;		//To maintain iterator in line
	int ch=0;		//word length
	argsCount=0;	//To maintain word count

	while(i < len)
	{
		//to skip any leading spaces
		while((line[i]==' ' || line[i] == '\t')&& i<len)
			i++;
		if(i!=0 && (line[i-1] == ' ' || line[i-1] == '\t' ) && ch!=0)
		{
			//Here terminate string and reset word length
			words[argsCount++][ch]='\0';
			ch=0;		
			continue;	
		}
		else
		{
			if(ch==0)
			{	//new word came so reallocate array
				//argsCount+2 because 1 entry is for '\0' and 2nd is for new word that came
				words = (char**)realloc(words,sizeof(char*)*(argsCount+2));
				words[argsCount+1]=	(char*)malloc(sizeof(char));
				words[argsCount+1][0]='\0';
			}
			//now store character
			words[argsCount] = (char*)realloc(words[argsCount], ch +2);
			words[argsCount][ch++]=line[i];		
		}
		i++;
	}

	if(ch!=0){			//to terminate last word
		words[argsCount++][ch]='\0';
	}

	words[argsCount]='\0';			//To detect words in loop
	return words;
}

char* mergeArgs(char** argList)
{
	char* cmd=(char*)malloc(1);
	cmd[0]='\0';

	if(argsCount>0)
	{
		strcpy(cmd,argList[0]);
		for(int i=1;i<argsCount;i++)
		{
			strcat(cmd," ");
			strcat(cmd,argList[i]);
		}
	}
	
	return cmd;
}