#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <functions.h>

extern int jNo;

int executeCmd(char** argList)
{
	int status;
	struct sigaction sa;
	int cpid=fork();
	int bg=isBackGround(argList);
	if(bg==-1)
	{
		fprintf(stderr,"Error: To run a process in backgroung & should be at the END.");
		return 1;
	}
	switch(cpid)
	{
		case -1:
			perror("Fork failed");
			exit(1);
			break;
		case 0:
			//All this is useless here because after exec new code will replace all this
			//So we can't write sig_handlers for childs that will exec
			// sa.sa_handler=&handleSigINT;
			// sa.sa_flags=SA_RESTART;
			// sigemptyset(&sa.sa_mask);
			// sigaction(SIGINT, &sa,NULL);		

			// signal(SIGINT,&handleSigINT);
			
			handleRedirection(argList);
			execvp(argList[0],argList);		//name of child(2nd parameter) will also be argList[0]
			perror("execvp failed");
			exit(1);
			break;
		default:
			if(bg==1)
			{
				fprintf(stdout,"[%d]\t%d\n",++jNo,cpid);
				sa.sa_sigaction=&sigCHILD;
				sa.sa_flags=SA_SIGINFO|SA_RESTART;
				sigaction(17, &sa,NULL);			//SIGCHILD=17
			}
			else
			{
				if(waitpid(cpid,&status,0)==-1)
					perror("waitpid failed");
				//to print '\n' if child is terminated by a signal
				handleSig(status);
			}
			return status;
	}
}

void handleSig(int status)		//To print \n after a process is terminated by SIGINT
{
	if (WIFSIGNALED(status))
		fprintf(stdout,"\n");
}

void handleSigINT(int sig)
{
	fprintf(stdout,"Caught signal =%d\n",sig);
	fprintf(stdout,"\n");
	exit(0);
}

void sigCHILD(int sig, siginfo_t *siginfo, void *context)
{
	int status;
	jNo--;
	pid_t pid=waitpid(-1, &status, WNOHANG);		//to wait for termination of any child
	if(pid>0)
		fprintf(stdout,"\nProcess: %d has terminated\n",pid);	
}