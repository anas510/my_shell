#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <signal.h>

char* inputString(char* , FILE* );
char** tokenize(char*);
char* mergeArgs(char**);

int executeCmd(char**);
int executePipe(char**);
char*** handlePipe(char**);		//it will return 2 char** lists
void handleRedirection(char**);

int isMultiple(char**);
int isPipeUsed(char**);
int isBackGround(char**);
char*** seperateCmds(char**);		//seperate cmds if written with ;

int getHistoryNumber(char*);
int isDigit(char);

void handleSig(int);
void handleSigINT(int);
void handleParentSig(int);
void sigCHILD(int , siginfo_t *, void *);

#endif