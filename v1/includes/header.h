#ifndef HEADER_H
#define HEADER_H

int inputLen=0;		//stores length of entered string
int argsCount=0;		//stores number of arguments in string found after parsing

char* inputString(char* , FILE* );
char** tokenize(char*);
int executeCmd(char**);
void handleSig(int);
void handleSigINT(int);
void handleParentSig(int);

#endif