#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <setjmp.h>
#include <header.h>

extern int inputLen;		//stores length of entered string
extern int argsCount;		//stores number of arguments in string found after parsing
static jmp_buf envbuf;

int main()
{
	//To make shell prompt
	char prompt[] = "cs321@";
	char pwd[4096];		//MAX Possible Linux PATH_LENGTH
	getcwd(pwd,4096);
	strcat(prompt,pwd);
	strcat(prompt,":- ");

	signal(SIGINT, &handleParentSig);			

	char* inputLine;
	char** argList;
	
	while(setjmp(envbuf)>=0 && (inputLine=inputString(prompt,stdin))!=NULL)
	{
		if((argList=tokenize(inputLine))!=NULL)
		{
			executeCmd(argList);

			//To free memory
			for(int i=0;i<argsCount;i++)
			{
				if(argList[i]!=NULL)
					free(argList[i]);
			}
			free(argList);
			free(inputLine);
			inputLen=0;
			argsCount=0;
		}
	}
	fprintf(stdout,"\n");
	return 0;	
}

//Why is it working only for 1st signal?
void handleParentSig(int sig)
{
	fprintf(stdout,"\n");
	signal(SIGINT, &handleParentSig);			
	longjmp(envbuf,sig);
}