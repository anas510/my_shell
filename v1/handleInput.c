#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

extern int inputLen;		//stores length of entered string
extern int argsCount;		//stores number of arguments in string found after parsing

char* inputString(char* prompt, FILE* fp_tty)
{
	fprintf(stdout,"%s",prompt);
    char* line = (char*)malloc(1);
    line[0] = '\0';   //this ensures that if user press enter without enterying anything the program do not crash
    char ch;
    inputLen = 0;
    do{
		//Store user character
		ch = getc(fp_tty);

		//whenever user press Enter key, break the loop
		if(ch == '\n')
		 break;
		else if(ch==EOF)
		{
			free(line);
			fprintf(stdout,"\n");
			exit(0);
		}
		line = (char*)realloc(line, inputLen +2);
		//the user enters a sentance, the program gets the input character by character
		// and the size keeps on growing dynamically as the user keeps entrying
		line[inputLen++] = ch;
    }while(ch!= '\n');

    line[inputLen] = '\0';
    return line;
}

char** tokenize(char* line)
{
	/*	allocate memory	*/
	if(inputLen==0)
		return NULL;
	char** words = (char**)malloc(sizeof(char*));		
	words[0] = (char*)malloc(sizeof(char));			//For 1st argument
	words[0][0]='\0';

	int len=strlen(line);
	int i=0;		//To maintain iterator in line
	int ch=0;		//word length
	argsCount=0;	//To maintain word count

	while(i < len)
	{
		//to skip any leading spaces
		while((line[i]==' ' || line[i] == '\t')&& i<len)
			i++;
		if(i!=0 && (line[i-1] == ' ' || line[i-1] == '\t' ) && ch!=0)
		{
			//Here terminate string and reset word length
			words[argsCount++][ch]='\0';
			ch=0;		
			continue;	
		}
		else
		{
			if(ch==0)
			{	//new word came so reallocate array
				//argsCount+2 because 1 entry is for '\0' and 2nd is for new word that came
				words = (char**)realloc(words,sizeof(char*)*(argsCount+2));
				words[argsCount+1]=	(char*)malloc(sizeof(char));
				words[argsCount+1][0]='\0';
			}
			//now store character
			words[argsCount] = (char*)realloc(words[argsCount], ch +2);
			words[argsCount][ch++]=line[i];		
		}
		i++;
	}

	if(ch!=0){			//to terminate last word
		words[argsCount++][ch]='\0';
	}

	words[argsCount]='\0';			//To detect words in loop
	return words;
}