#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <functions.h>

extern int argsCount;

char*** handlePipe(char** argList)
{
	char*** cmds=(char***)malloc(sizeof(char**)*2);
	cmds[0]=argList;
	int scnd=0;
	int j=0,k=0;
	for(int i=0;i<argsCount;i++)
	{
		if(scnd==1)
		{
			cmds[1][j++]=argList[i];
		}
		else if(scnd==0 && strstr(argList[i],"|")!=NULL)		//input redirection
		{
			k=i;
			scnd=1;
			cmds[1]=(char**)malloc(sizeof(char*)*(argsCount-i));
			j=0;
		}
	}
	cmds[0][k]='\0';		//terminate 1st cmd args at |
	cmds[1][j]='\0';
	// fprintf(stderr,"k = %d, argsCount = %d, j = %d",k,argsCount,j);
	return cmds;
}

int executePipe(char** argList)
{
	int status;
	int fd[2];
	if(pipe(fd)<0)		//opens 2 descriptors for pipe 	fd[0] = input, fd[1]=output
	{
		perror("Pipe failed");
		return 1;
	}

	char*** cmds=handlePipe(argList);
	int cpid=fork();
	switch(cpid)
	{
		case -1:
			perror("Fork failed");
			exit(1);
			break;
		case 0:
			//shell handles pipe before redirection because if we run command with both redirection and pipe
			//output will not goto 2nd command as it will be redirected
			//REDIRECT OUTPUT of 1st 2 fd[1]
			dup2(fd[1],1);
			//Then check for IO-Redirection
			handleRedirection(cmds[0]);
			execvp(cmds[0][0],cmds[0]);		//name of child(2nd parameter) will also be cmds[0][0]
			perror("execvp failed");
			exit(1);
			break;
		default:
			if(waitpid(cpid,&status,0)==-1)
				perror("waitpid failed");
			handleSig(status);
			//when child terminates then fork for 2nd command
			cpid=fork();
			//close fd[1] otherwise next command will be blocked in waiting forever
			close(fd[1]);
			switch(cpid)
			{
				case -1:
					perror("Fork failed");
					exit(1);
					break;
				case 0:
					//REDIRECT INPUT of 2nd 2 fd[0]
					dup2(fd[0],0);
					//Then check for IO-Redirection
					handleRedirection(cmds[1]);
					execvp(cmds[1][0],cmds[1]);		//name of child(2nd parameter) will also be cmds[1][0]
					perror("execvp failed");
					exit(1);
					break;
				default:
					if(waitpid(cpid,&status,0)==-1)
						perror("waitpid failed");
					handleSig(status);
					close(fd[0]);
			}
	}

	//free only 2nd list{cmds[1]} as 1st will be deallocated with argList
	if(cmds[1]!=NULL)
		free(cmds[1]);
	free(cmds);
	return status;
}