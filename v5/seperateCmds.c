#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <functions.h>

extern int argsCount;

char*** seperateCmds(char** argList)
{
	int size=1;
	char*** cmds=(char***)malloc(sizeof(char**)*(size+1));
	cmds[0]=argList;
	cmds[1]='\0';
	
	if(isMultiple(argList)==0)
		return cmds;

	int k=0,j=0;
	for(int i=0;i<argsCount;i++)
	{
		if(strstr(argList[i],";")!=NULL)
		{
			cmds[k][j]='\0';
			j=0;
			if(i!=argsCount-1)		//input redirection
			{
				cmds=(char***)realloc(cmds,sizeof(char**)*(++size+1));
				cmds[++k]=&argList[i+1];
				cmds[k+1]='\0';
			}
		}
		else
			j++;
	}
	return cmds;
}

int isMultiple(char** argList)
{
	int i=0;
	while(argList[i]!=NULL)
	{
		if(strstr(argList[i],";")!=NULL)
		{
			if(i!=argsCount-1)
			{
				return 1;
			}
			else
				return -1;
		}
		i++;
	}
	return 0;
}

char*** extractCmds(char** argList)
{
	//fi is not at the end, then is at 2nd arg =>  ERROR
	if(argsCount<5 || strstr(argList[1],"then")!=NULL || strstr(argList[argsCount-1],"fi")==NULL ||
	 (strstr(argList[argsCount-2],"fi")==NULL && strstr(argList[argsCount-1],";")!=NULL))		
		return NULL;

	int i=1;
	short then=0,els=0;
	while(argList[i]!=NULL)
	{
		if(strstr(argList[i],"then")!=NULL)
		{
			if(i>=argsCount-2)		//should be at argsCount-3
				return NULL;
			then=1;
		}
		else if(strstr(argList[i],"else")!=NULL)
		{
			if(i>=argsCount-2)
				return NULL;
			els=1;
		}
		i++;
	}

	if(then==0 || (els==1 && argsCount<7))
		return NULL;

	int size=2;
	if(els==1)
		size++;
	char*** cmds=(char***)malloc(sizeof(char**) * (size+1));
	cmds[0]=&argList[1];
	cmds[size]='\0';
	
	int j=0,s=0;
	for(int i=1; i<argsCount && s<size;i++)
	{
		if(s==0 && strstr(argList[i],"then")!=NULL)
		{
			cmds[s][j]='\0';
			j=0;
			cmds[++s]=&argList[i+1];
		}
		else if(s==1 && (strstr(argList[i],"fi")!=NULL || strstr(argList[i],"else")!=NULL))
		{
			cmds[s][j]='\0';
			j=0;
			if(els==1)
				cmds[++s]=&argList[i+1];
		}
		else if(els==1 && s==2 && strstr(argList[i],"fi")!=NULL)
		{
			cmds[s][j]='\0';
			j=0;
			break;
		}
		else
			j++;
	}
	cmds[size]=NULL;
	return cmds;
}